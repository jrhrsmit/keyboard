OUT := output
OUT_MODELS := $(OUT)/models
LOGFILE := $(OUT)/transcript.log

.PHONY: keyboard plate_mid plate_top plate_top_white display hide_references clean

default: keyboard plate_mid plate_top plate_top_white display

$(OUT):
	mkdir -p $(OUT)

$(OUT_MODELS):
	mkdir -p $(OUT_MODELS)

keyboard: $(OUT)
	kikit fab jlcpcb --assembly --schematic $@/$@.kicad_sch $@/$@.kicad_pcb $(OUT)/$@ | tee -a $(LOGFILE)

plate_mid: $(OUT)
	kikit fab jlcpcb $@/$@.kicad_pcb $(OUT)/$@ | tee -a $(LOGFILE)
	kikit fab jlcpcb $@/$@_alps.kicad_pcb $(OUT)/$@_alps | tee -a $(LOGFILE)
	kikit fab jlcpcb $@/$@_hybrid.kicad_pcb $(OUT)/$@_hybrid | tee -a $(LOGFILE)

plate_mid_steel_step: plate_mid $(OUT)
	python3 scripts/kicadpcb2dxf.py -f plate_mid/plate_mid.kicad_pcb
	mv plate_mid/plate_mid.dxf $(OUT)
	freecadcmd scripts/dxf_to_step.py

plate_mid_hybrid_steel_step: plate_mid $(OUT)
	python3 scripts/kicadpcb2dxf.py -f plate_mid/plate_mid_hybrid.kicad_pcb
	mv plate_mid/plate_mid_hybrid.dxf $(OUT)/plate_mid.dxf
	freecadcmd scripts/dxf_to_step.py
	mv $(OUT)/plate.stp $(OUT)/plate_mid_hybrid.stp

plate_top: $(OUT)
	kikit fab jlcpcb --assembly --schematic $@/$@.kicad_sch $@/$@.kicad_pcb $(OUT)/$@ | tee -a $(LOGFILE)

plate_top_white: $(OUT)
	kikit fab jlcpcb --assembly --schematic $@/$@.kicad_sch $@/$@.kicad_pcb $(OUT)/$@ | tee -a $(LOGFILE)

display: $(OUT)
	kikit fab jlcpcb --assembly --schematic $@/$@.kicad_sch $@/$@.kicad_pcb $(OUT)/$@ | tee -a $(LOGFILE)

models: $(OUT_MODELS)
	kicad2step -o $(OUT_MODELS)/keyboard.stp keyboard/keyboard.kicad_pcb
	kicad2step -o $(OUT_MODELS)/plate_mid.stp plate_mid/plate_mid.kicad_pcb
	kicad2step -o $(OUT_MODELS)/plate_top.stp plate_top/plate_top.kicad_pcb
	kicad2step -o $(OUT_MODELS)/display.stp display/display.kicad_pcb --user-origin=-328.62x-85.72mm

hide_references:
	kikit modify references --hide --pattern ".*" keyboard/keyboard.kicad_pcb
	kikit modify references --hide --pattern ".*" display/display.kicad_pcb
	kikit modify references --hide --pattern ".*" plate_top/plate_top.kicad_pcb
	kikit modify references --hide --pattern ".*" plate_mid/plate_mid.kicad_pcb
	kikit modify references --hide --pattern ".*" plate_mid/plate_mid_alps.kicad_pcb

clean:
	rm -rf $(OUT) || true
