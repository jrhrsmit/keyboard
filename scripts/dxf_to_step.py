import FreeCAD
import FreeCAD
import Part
import Path
import importDXF

import DraftGeomUtils
import Draft
from DraftGeomUtils import geomType
import OpenSCAD2Dgeom

DOC_NAME = "plate"
thickness_mm = 1.5
create_face_object = False
project_filename = f"output/{DOC_NAME}.FcStd"
output_filename = f"output/{DOC_NAME}.stp"

# create document
FreeCAD.newDocument(DOC_NAME)
FreeCAD.setActiveDocument(DOC_NAME)
doc = FreeCAD.activeDocument()

# import DXF
importDXF.insert(u"output/plate_mid.dxf", "plate")

# change to face
edges = sum((obj.Shape.Edges for obj in
             FreeCAD.ActiveDocument.Objects if hasattr(obj, 'Shape')), [])

# set higher tolerance
for e in edges:
    for v in e.Vertexes:
        v.Tolerance = 0.1

face = OpenSCAD2Dgeom.edgestofaces(edges)
face.check()
face.fix(0, 0, 0)
if create_face_object:
    faceobj = doc.addObject('Part::Feature', "face_dxf")
    faceobj.Label = "face_dxf"
    faceobj.Shape = face

# extrude
extr = face.extrude(FreeCAD.Vector(0, 0, thickness_mm))
extrobj = doc.addObject('Part::Extrusion', 'Extrude')
extrobj.Shape = extr
extrobj.Label = "Extrude"
extrobj.Visibility = True
doc.getObject("Extrude").Visibility = True

# delete all imported dxf lines
i = 0
for obj in FreeCAD.ActiveDocument.Objects:
    if obj.Name.startswith("Shape"):
        doc.removeObject(obj.Name)
        i = i + 1
print(f"Deleted {i} imported DXF lines")

# save file
doc.saveAs(project_filename)
print(f"Saved document in {output_filename}")

# export STEP
objs = [doc.getObject("Extrude")]
Part.export(objs, output_filename)

print("Done")
