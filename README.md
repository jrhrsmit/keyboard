# Keyboard
A JLCPCB optimized keyboard, consisting only of PCBs, screws, and standoffs, by Jasper Smit.

## Features
 - ANSI 87-key layout
 - Support for hot-swap Cherry-type keys
 - Support for (specifically Matias) Alps
 - RGB backlight using WS2818B and low-profile RGB LEDs
 - USB type-C
 - Bluetooth
 - 450mAh battery
 - Can run LEDs on battery with boost converter (but will drain fast)
 - Capacitive proximity sensor
 - [JLCPCB](https://jlcpcb.com/) optimized
 - Running [ZMK](https://github.com/zmkfirmware/zmk) ([forked](https://github.com/jrhrsmit/zmk))
 - Using a Nordic nRF52840

Although it is "JLCPCB optmized", it does require hand-soldering of the
nRF52840, the switches, and connectors.

## Mechanical buildup

### Possible choices for spacers
washers:
 - nylon 1.0 mm
 - steel 0.5 mm https://www.aliexpress.com/item/4001297092416.html
 - wide steel 1.0 mm
 - nylon 1.2 mm https://www.aliexpress.com/item/1005002717490479.html
    https://www.aliexpress.com/item/1005003307102449.html

nuts:
 - nylon 2.8 mm https://www.aliexpress.com/item/1005002182296958.html
 - nylon 2.5 mm https://www.aliexpress.com/item/1005002328272789.html
 - nylon 2.4 mm https://www.aliexpress.com/item/32967736647.html

### Cherry MX
Desired distance between `keyboard` and `plate_mid` = 3.4 mm
3mm spacers

nylon 2.4mm nut + nylon 1.0 mm washer.

Cherry MX keyboard:
 - 2.4 mm nut:
 https://www.aliexpress.com/item/32967736647.html (nylon, black, 50pcs)
 - 1.0 mm washer:
 https://www.aliexpress.com/item/1005002717490479.html (nylon, black, 200pcs)
 - 6.0 mm spacer:
 https://www.aliexpress.com/item/32778481352.html (aluminium, black, 50pcs)
 https://www.aliexpress.com/item/32858416975.html (nylon, black, 50pcs)
 https://www.aliexpress.com/item/32422269228.html (nickel plated brass, 100pcs)
 - 10.0 mm M3 screw flat head:
 https://www.aliexpress.com/item/1005003571169393.html (steel, black/silver, 50pcs)
 - 4.0 mm M3 screw flat head:
 https://www.aliexpress.com/item/1005003571169393.html (steel, black/silver, 50pcs)

One keyboard needs 18 sets.

### Alps
Desired distance between `keyboard` and `plate_mid` = 2.9 + 0.85 mm
(+-0.10 mm) = 3.75 mm

nylon 2.8 mm nut + nylon 1.0 mm washer.

Alps keyboard:
 - 2.8 mm nut: 
 https://www.aliexpress.com/item/1005002182296958.html (nylon, black, 50pcs)
 - 1.0 mm washer:
 https://www.aliexpress.com/item/1005002717490479.html (nylon, black, 200cps)
 - 6.0 mm spacer:
 https://www.aliexpress.com/item/32778481352.html (aluminium, black, 50pcs)
 https://www.aliexpress.com/item/32858416975.html (nylon, black, 50pcs)
 https://www.aliexpress.com/item/32422269228.html (nickel plated brass, 100pcs)
 https://www.aliexpress.com/item/32960399166.html
 - 10.0 mm M3 screw flat head:
 https://www.aliexpress.com/item/1005003571169393.html (steel, black/silver, 50pcs)
 - 4.0 mm M3 screw flat head:
 https://www.aliexpress.com/item/1005003571169393.html (steel, black/silver, 50pcs)

One keyboard needs 18 sets.

### Display module

0.91" OLED module 128x32 pixels, with SSD1306 driver.
https://www.aliexpress.com/item/32836691660.html

Screws (2x1.6 + 3.0 + 1.6) = 7.8, 8.0mm.



### old stuff

The board consists of three PCBs:
 - `keyboard`, the only PCB that contains components and which houses the MCU
   and the switches
 - `plate_mid`, the middle plate (in other keyboards usually made out of metal)
 - `palte_top`, the top plate, which is also used for capacitive proximity
   sensing.

These PCBs are stacked using screws, standoffs, and washer rings.
There are two types of mechanical connections:
 - From `keyboard` to `plate_mid`
 - All layers (from `keyboard`, through `plate_mid`, to `plate_top`)

The first mechanical stack looks like this:
 - nylon bolt (2.8mm)
 - `keyboard` PCB (1.6mm)
 - washer (0.4mm)
 - standoff (3.0mm)
 - `plate_mid` (1.6mm)
 - screw (M2x10mm, flat head)
These type of connections are used under the keys in the keyboard, and are
probably not really necessary as the keys themselves also clamp to both layers.

The second mechanical stack looks like this:
 - screw (M2x10mm)
 - `keyboard` PCB (1.6mm)
 - washer (0.4mm)
 - standoff (3.0mm)
 - `plate_mid` (1.6mm)
 - standoff (6.0mm)
 - `plate_top` (1.6mm)
 - screw (M2x4mm)
These are used wherever possible on the top plate, on 18 locations.
The two screws are connected by the 6mm standoff.
These screws also provide the electrical connection to the planes used for
capacitive proximity sensing in `plate_top`.

### Alps keys with RGB
The keyboard can combine Matias Alps switches with the RGB LEDs on the board.
Because the Matias Alps switches do not have room for a LED, the entire switch
is elevated by about 0.2mm using surface-mounted resistors.
These resistors are placed in every corner of the switch, just outside the
footprint of the Cherry switches.

This provides another advantage; namely that the `plate_mid` can be at exactly
the same height for Cherry and Alps switches.

Note that only Matias Alps switches have been tested for this keyboard, no
guarantee can be provided for any other Alps switch.

## Capacitive proximity sensor
The capacitive sensor consists of the entire top layer of `plate_top`.
The bottom layer of `plate_top` is driven at the exact same voltage as the top
layer using a voltage follower, this ensures that there is no charge to the
ground planes on the `keyboard` below, and thus acts as a guard.

The capacitance is measured by the nRF52 by charging up the capacitor and
letting it slowly discharge via a 3MOhm pulldown resistor.
The rate of discharge then determines the capacitance.

```
                                      voltage follower

                                  +---------------------+                           plate_top
+------------+     +----------+   |                     |      +---------------------------------------------------+
|            |     |          |   |    +-               |      |                                                   |
|   nRF52    |     |  3MOhm   |   |    |  `             +------+   +----------------+    Capacitive sensor plane   |
|   LPCOMP   +-----+ pulldown +---+----+ +   `                 |                                                   |
|            |     |          |        |       >--------+------+   +----------------+    Guard plane               |
|            |     |          |   +----+ -   .          |      |                                                   |
+------------+     +----------+   |    |  .             |      +---------------------------------------------------+
                                  |    +-               |
                                  |                     |
                                  +---------------------+
```

### Software
The capacitor is charged by the internal 13kOhm pull-up of the nRF52 (writing
directly to the GPIO registers as this is not supported in the drivers).
The timer and low-power comparator can be enabled while the capacitor is
charging up (this only takes a few microseconds).
The pull-up is then released, letting the capacitor slowly discharge.
The `NRF_LPCOMP_EVENT_DOWN` is then triggered when 2/8th of VDD is reached on
the pin, which stops the timer via the PPI module.
The use of the PPI module improves the accuracy of the measurement, as no
context switching or CPU time is required to stop the timer.

Multiple samples are taken to improve accuracy.
All sample values are sorted, and only the middle 10%-90% are averaged and used
as the final capacitance value.
The resulting value is then compared against the last values, and if it is
higher than `AVG(latest values) + THRESHOLD` then the sensor event is triggered
and the keyboard wakes up (and enables backlight).

To avoid stalling the CPU by sampling the sensor, the sensor is only active
while the keyboard is idle.

For now a fork is maintained on [GitHub](https://github.com/jrhrsmit/zmk) which
implements this functionality.

## Revisions
### v1.0
Prototype, not recommended for anyone.
### v1.1
To be released.
